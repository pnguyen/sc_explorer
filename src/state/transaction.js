import { ACTIONS, makeFetchAction } from 'redux-api-call';
import { handleActions } from 'redux-actions';
import { path } from 'lodash/fp';
import { combineReducers } from "redux";
import {EXPLORER_API, GET_TRANSACTION, GET_TRANSACTION_LIST} from "../constants";
import { timeSince } from "./block";

export const fetchTransactionListAC = makeFetchAction(GET_TRANSACTION_LIST, (offset, limit, order) => ({
    endpoint: () => {
        let url = `${EXPLORER_API}/api/transactions?limit=${limit > 0 ? limit : 10}`;
        if (offset && offset !== "") url += `&offset=${offset}`;
        if (order !== 1) order = 0;
        url += `&order=${order}`;
        return url;
    }
}));

export const fetchTransactionInfoAC = makeFetchAction(GET_TRANSACTION, () => {
    let { pathname } = window.location;
    if (pathname.indexOf("transaction/") > -1) {
        pathname = pathname.replace("transaction/", "");
    }
    return {
        endpoint: `${EXPLORER_API}/api/transaction/${pathname}`
    }
});

export const transactionListReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_TRANSACTION_LIST) {
            let txData = path("json", payload);
            let results = [];
            txData.forEach(tx => {
                results.push([
                    {
                        urls: [
                            {
                                url: `${window.location.origin}/transaction/${tx.transactionHash}`,
                                text: tx.transactionHash.slice(0, 10) + "...",
                            }
                        ]
                    },
                    {
                        urls: [
                            {
                                url: `${window.location.origin}/block/${tx.blockHeight}`,
                                text: tx.blockHeight,
                            }
                        ]
                    },
                    timeSince(tx.time.seconds),
                    tx.from,
                ]);
            });
            return results;
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_TRANSACTION_LIST) {
            console.log(`handle getTransactionList failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const transactionReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_TRANSACTION) {
            return path("json", payload);
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_TRANSACTION) {
            console.log(`handle getTransactionList failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const transactionReducers = combineReducers({
    list: transactionListReducer,
    info: transactionReducer
});