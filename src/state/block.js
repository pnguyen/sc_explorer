import { ACTIONS, makeFetchAction } from 'redux-api-call';
import { handleActions } from 'redux-actions';
import { path } from 'lodash/fp';
import { combineReducers } from "redux";
import {EXPLORER_API, GET_BLOCK_LIST} from "../constants";

export const toDateTime = (secs) => {
    let t = new Date(1970, 0, 1); // Epoch
    t.setSeconds(secs);
    return t;
};

function calculateTime(seconds) {
    if (seconds <= 0) return "";
    if (seconds < 60) {
        return seconds + ' seconds';
    }
    if (seconds < 3600) {
        let min = Math.floor(seconds / 60);
        return min + ' minutes ' + calculateTime(Math.floor(seconds%60));
    }
    if (seconds <= 86400) {
        let hours = Math.floor(seconds / 3600);
        return hours + ' hours';
    }
    if (seconds > 86400) {
        let d = toDateTime(seconds);
        let now = new Date();
        let day = d.getDate();
        let month = d.toDateString().match(/ [a-zA-Z]*/)[0].replace(" ", "");
        let year = d.getFullYear() === now.getFullYear() ? "" : " " + d.getFullYear();
        return day + " " + month + year;
    }
}

export const timeSince = (seconds) => {
    let now = new Date(),
        secondsPast = Math.floor(now.getTime()/1000) - seconds;
    return calculateTime(secondsPast);
};

export const fetchBlockListAC = makeFetchAction(GET_BLOCK_LIST, (offset, limit, order) => ({
    endpoint: () => {
        let url = `${EXPLORER_API}/api/blocks?limit=${limit > 0 ? limit : 10}`;
        if (offset && offset !== "") url += `&offset=${offset}`;
        if (order !== 1) order = 0;
        url += `&order=${order}`;
        return url;
    }
}));

export const blockListReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_BLOCK_LIST) {
            let data = path("json", payload);
            let results = [];
            data.forEach(block => {
                results.push([
                    {
                        urls: [
                            {
                                url: `${window.location.origin}/block/${block.height}`,
                                text: block.height,
                            }
                        ]
                    },
                    timeSince(block.time.seconds),
                    block.validator,
                    block.numTxs ? block.numTxs : 0
                ]);
            });
            return results;
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_BLOCK_LIST) {
            console.log(`handle getBlockList failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const blockReducers = combineReducers({
    list: blockListReducer
});