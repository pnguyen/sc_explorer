import React from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'lodash/fp';
import {
    BLOCK, TRANSACTION
} from "../../constants";
import ReactInterval from 'react-interval';

// core components
import GridItem from '../../components/Grid/GridItem.jsx';
import GridContainer from '../../components/Grid/GridContainer.jsx';
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "../../assets/jss/material-dashboard-react/views/dashboardStyle";
// view components
import {fetchBlockListAC} from "../../state/block";
import {fetchTransactionListAC} from "../../state/transaction";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import Success from "../../components/Typography/Success";
import Danger from "../../components/Typography/Danger";
import TablePagination from "../../components/Table/TablePagination";
import Info from "../../components/Typography/Info";
import Button from "../../components/CustomButtons/Button";


export const List = ({ height, label, tableHead, tableData, tableHeaderColor, length, detailsUri="" }) => (
    <Card style={{ height: height, overflowY: "auto", overflowX: "auto"}}>
        <CardBody>
            { tableHeaderColor === "success" ?
                <Success style={{ marginLeft: 15, marginTop: 15 }}>
                    <b>{label}</b>
                </Success> : ''
            }
            { tableHeaderColor === "danger" ?
                <Danger style={{ marginLeft: 15, marginTop: 15 }}>
                    <b>{label}</b>
                </Danger> : ''
            }
            { tableHeaderColor === "info" ?
                <Info style={{ marginLeft: 15, marginTop: 15 }}>
                    <b>{label}</b>
                </Info> : ''
            }

            <hr style={{
                marginTop: ".75rem",
                marginBottom: ".75rem",
                opacity: ".5",
                marginLeft: "-20px",
                marginRight: "-20px"
            }}/>

            <TablePagination tableHeaderColor={tableHeaderColor}
                             tableHead={tableHead}
                             tableData={tableData}
                             length={length}
            />

            <div style={{ width: "70px", marginLeft: "auto", marginRight: "auto", marginTop: "30px" }}>
                <Button color={tableHeaderColor} href={`${window.location.origin}/${detailsUri}`} size="sm">
                    More
                </Button>
            </div>
        </CardBody>
    </Card>
);


export const BlocksTransactionsLayout = ({ blocksList, transactionsList, getBlockList, getTransactionList }) => (
    <>
        <GridContainer>
            <GridItem xs={6} sm={6} style={{ marginTop: -40 }}>
                { !blocksList || blocksList.length === 0 ? <List height={700} label={"Recent Blocks"} tableHead={["Height", "Time", "Validator", "NumTxs"]} tableData={[]} tableHeaderColor={"success"}/> :
                    <List height={700} label={"Recent Blocks"} tableHead={["Height", "Time", "Validator", "NumTxs"]} tableData={blocksList} tableHeaderColor={"success"} length={10} detailsUri="blocks"/>
                }
            </GridItem>
            <GridItem xs={6} sm={6} style={{ marginTop: -40 }}>

                { !transactionsList || transactionsList.length === 0 ? <List height={700} label={"Recent Transactions"} tableHead={["Hash", "Height", "Time", "From"]} tableData={[]} tableHeaderColor={"info"}/> :
                    <List height={700} label={"Recent Transactions"} tableHead={["Hash", "Height", "Time", "From"]} tableData={transactionsList} tableHeaderColor={"info"} length={10} detailsUri="transactions"/>
                }
            </GridItem>
        </GridContainer>
        { <ReactInterval timeout={2000} enabled={true} callback={() => getBlockList()}/> }
        { <ReactInterval timeout={2000} enabled={true} callback={() => getTransactionList()}/> }
    </>
);

export const BlocksTransactions = compose(
    connect(
        state => ({
            blocksList: path(BLOCK.DOT_BLOCKS_LIST)(state),
            transactionsList: path(TRANSACTION.DOT_TRANSACTIONS_LIST)(state)
        }),
        {
            getBlockList: fetchBlockListAC.actionCreator,
            getTransactionList: fetchTransactionListAC.actionCreator,
        }
    ),
    withStyles(dashboardStyle)
)(BlocksTransactionsLayout);

export default () => (
    <>
        <BlocksTransactions />
    </>
);
