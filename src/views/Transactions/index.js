import Card from '../../components/Card/Card.jsx';
import CardBody from '../../components/Card/CardBody.jsx';
import Muted from '../../components/Typography/Muted';
import Info from "../../components/Typography/Info";
import React from "react";
import {compose} from "recompose";
import {connect} from "react-redux";
import {path} from "lodash/fp";
import {TRANSACTION} from "../../constants";
import {lifecycle} from "recompose/dist/Recompose.cjs";
import {fetchTransactionInfoAC} from "../../state/transaction";
import {toDateTime} from "../../state/block";

const hrSpace = {
    marginTop: ".75rem",
    marginBottom: ".75rem",
    opacity: ".75"
};

const TransactionLayout = ({tx, getTransaction}) => (
    <>
        <Muted><h2>Transaction Details</h2></Muted>
        <Card>
            <CardBody>
                <Info><b>Transaction Hash</b></Info>
                <Muted>{tx.transactionHash}</Muted>

                <hr style={hrSpace} />

                <Info><b>Block Height</b></Info>
                <Info><a href={`${window.location.origin}/block/${tx.blockHeight}`}>{tx.blockHeight}</a></Info>

                <hr style={hrSpace} />

                <Info><b>Nonce</b></Info>
                <Muted>{tx.nonce}</Muted>

                <hr style={hrSpace} />

                <Info><b>Time</b></Info>
                <Muted>{tx.time ? toDateTime(tx.time.seconds).toUTCString() : 0}</Muted>

                <hr style={hrSpace} />

                <Info><b>OrganizationId</b></Info>
                <Muted>{tx.organizationId}</Muted>

                <hr style={hrSpace} />

                <Info><b>Organization Hash</b></Info>
                <Muted>{tx.idHash}</Muted>

                <hr style={hrSpace} />

                <Info><b>Method</b></Info>
                <Muted>{tx.method}</Muted>

                <hr style={hrSpace} />

                <Info><b>Sender</b></Info>
                <Muted>{tx.from}</Muted>

                <hr style={hrSpace} />

                <Info><b>To</b></Info>
                <Muted>{tx.to}</Muted>

                <hr style={hrSpace} />

                <Info><b>Payload</b></Info>
                <Muted>{tx.payload}</Muted>

                <hr style={hrSpace} />

                <Info><b>Signature</b></Info>
                <Muted>{tx.signature}</Muted>

                <hr style={hrSpace} />

            </CardBody>
        </Card>
    </>
);

const Transaction = compose(
    connect(
        state => ({
            tx: path(TRANSACTION.DOT_TRANSACTION_INFO)(state)
        }),
        {
            getTransaction: fetchTransactionInfoAC.actionCreator,
        }
    ),
    lifecycle({
        componentDidMount() {
            this.props.getTransaction();
        }
    }),
)(TransactionLayout);

export default () => (
    <div >
        <Transaction />
    </div>
);

