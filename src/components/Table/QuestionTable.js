import React from "react";
import {
  Table,
  TableRow,
  TableCell,
  TableHead,
  TableBody,
  Tooltip,
  IconButton,
} from "@material-ui/core";
import CircularProgress from '@material-ui/core/CircularProgress';
import { Edit } from "@material-ui/icons";
import { map, toString } from "lodash/fp";
import { connect } from "react-redux";
import { answerQuestionFA } from "../../views/NEO_ETH/state";
import { TextField } from "redux-form-material-ui";
import { Field, reduxForm, getFormValues } from "redux-form";
import {config, PRIVATE_A_ORG_ID, PRIVATE_B_ORG_ID, blockLink} from "../../constants";
import { compose, withState, withStateHandlers } from  'recompose';

const QuestionRow = ({ classes, itemData, form, currentOrgID }) => (
  <TableRow key={itemData.id}>
    <TableCell className={classes.tableCell}>{itemData.org}</TableCell>
    <TableCell className={classes.tableCell}>
      {
        itemData.blockAdded > 0 ?
        <a href={blockLink(itemData.blockAdded, currentOrgID)} rel={"noopener noreferrer"} target={"_blank"}>{itemData.blockAdded}</a> :
        '-'
      }
    </TableCell>
    <TableCell className={classes.tableCell}>{itemData.email}</TableCell>
    <TableCell>
      <Field name={"questionName"} component={TextField} />
    </TableCell>
    <TableCell className={classes.tableActions}>
      <ConnectedButton form={form} classes={classes} email={itemData.email} orgID={itemData.org} currentOrgID={currentOrgID} qid={itemData.id} />
    </TableCell>
  </TableRow>
);

const processOnclick = async (fetchData, value, email, orgID, currentOrgID, qid, onLoading, onCloseLoading) => {
  let endpoint = '';
  switch (currentOrgID) {
    case PRIVATE_A_ORG_ID:
      endpoint = config.host.A;
      break;
    case PRIVATE_B_ORG_ID:
      endpoint = config.host.B;
      break;
    default:
      throw "invalid org id";
  }
  onLoading();
  await fetchData(endpoint, qid, email, value.questionName, orgID);
  onCloseLoading();
};

const Button = ({ classes, fetchData, value, email, orgID, currentOrgID, qid, loading, onLoading, onCloseLoading }) => (
  <Tooltip
    id="tooltip-top"
    title="Answer"
    placement="top"
    classes={{ tooltip: classes.tooltip }}
  >
    { loading ? <CircularProgress size={24} /> :
      <IconButton
        aria-label="Edit"
        className={classes.tableActionButton}
        onClick={() => processOnclick(fetchData, value, email, orgID, currentOrgID, qid, onLoading, onCloseLoading)}
      >
        <Edit className={classes.tableActionButtonIcon + " " + classes.edit}/>
      </IconButton>
    }
  </Tooltip>
);

const ConnectedButton = compose(
  connect(
    (state, { form }) => ({
      value: getFormValues(form)(state),
    }),
    {
      fetchData: answerQuestionFA.actionCreator
    }
  ),
  withState({
    loading: false
  }),
  withStateHandlers(() => ({ loading: false }), {
    onLoading: ({ loading }) => () => ({
      loading: true
    }),
    onCloseLoading: ({ loading }) => () => ({
      loading: false
    })
  })
  )(Button);

const FormQuestionRown = reduxForm()(QuestionRow);

export default ({ classes, tableData, currentOrgID, tableHeaderColor="gray" }) => (
  <div className={classes.tableResponsive}>
    <Table className={classes.table}>
      <TableHead  className={classes[tableHeaderColor + "TableHeader"]}>
        <TableRow>
          <TableCell className={classes.tableCell + " " + classes.tableHeadCell}>OrgID</TableCell>
          <TableCell className={classes.tableCell + " " + classes.tableHeadCell}>Added Block</TableCell>
          <TableCell className={classes.tableCell + " " + classes.tableHeadCell}>Content</TableCell>
          <TableCell className={classes.tableCell + " " + classes.tableHeadCell}>Answer</TableCell>
          <TableCell className={classes.tableCell + " " + classes.tableHeadCell}>Submit</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {map(
          itemData => (
            <FormQuestionRown
              key={itemData.id}
              itemData={itemData}
              classes={classes}
              form={toString(itemData.id)}
              currentOrgID={currentOrgID}
            />
          ),
          tableData
        )}
      </TableBody>
    </Table>
  </div>
);
