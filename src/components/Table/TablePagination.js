import React from "react";
import PropTypes from "prop-types";
import { compose, defaultProps, setPropTypes, withStateHandlers, lifecycle, withState } from "recompose";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// core components
import tableStyle from "assets/jss/material-dashboard-react/components/tableStyle.jsx";
import Button from '../CustomButtons/Button.jsx';

export const Link = ({urls}) => {
  if (urls.length > 1)
    return (
      <div style={{ overflowY: "auto", msOverflowY: "auto", maxHeight: 50, height: 25 }}>
        {
          urls.map((prop, key) => {
            return (
              <p style={{ marginTop: 10 }}><a rel={"noopener noreferrer"} href={prop.url}>{prop.text}</a></p>
            )
          })
        }
      </div>
    );

  return (
    <a rel={"noopener noreferrer"} href={urls[0].url}>{urls[0].text}</a>
  );

};

const TablePaginationLayout = ({ classes, tableHead, tableData, tableHeaderColor, length, totalPage, currentData, currentPage, switchPage }) => (
  <div className={classes.tableResponsive}>
    <Table className={classes.table}>
      {tableHead !== undefined ? (
        <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
          <TableRow>
            {tableHead.map((prop, key) => {
              return (
                <TableCell
                  className={classes.tableCell + " " + classes.tableHeadCell}
                  key={key}
                >
                  {prop}
                </TableCell>
              );
            })}
          </TableRow>
        </TableHead>
      ) : null}
      {(currentData !== null && currentData !== undefined && currentData.length !== 0) ?
        <TableBody>
          {
            currentData.map((prop, key) => {
              return (
                <TableRow key={key}>
                  {prop.map((prop, key) => {
                    return (
                      <TableCell className={classes.tableCell} key={key}>
                        {
                          typeof prop === 'object' && prop["urls"] !== undefined ?
                            <Link urls={prop["urls"]}/> : prop
                        }
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })
          }
        </TableBody>
      : null}
    </Table>
    { currentData === null || currentData === undefined || currentData.length === 0 ? <p>No data found.</p> : ''}
    <table style={{ border: "None", width: "100%", position: "absolute", right: 20, bottom: 10 }}>
      <tbody>
        <tr>
          <td/>
          <td align={"right"}>
            { currentPage !== 0 ?
              <Button onClick={() => {switchPage(currentPage - 1, tableData, length)}} variant="contained" color="success" size="sm">
                Previous
              </Button> : ''
            }
            { currentPage >= 0 && currentPage < calculateTotalPage(tableData, length) && calculateTotalPage(tableData, length) > 1 ?
              <Button onClick={() => {switchPage(currentPage + 1, tableData, length)}} variant="contained" color="success" size="sm">
                Next
              </Button> : ''
            }
          </td>
        </tr>
      </tbody>
    </table>
  </div>
);

const calculateTotalPage = (tableData, length) => {
  if (tableData.length === 0) return 0;
  else if (tableData.length <= length) return 1;
  return Math.ceil(tableData.length / length);
};

const getDataAndPage = (page, data, length) => {
  let currentData = [];
  let currentPage = 0;
  const totalPage = calculateTotalPage(data, length);
  if (data.length !== 0 && page < totalPage) {
    currentData = data.slice(page*length, (page+1)*length);
    currentPage = page;
  } else {
    currentData = [];
    currentPage = page;
  }
  return [currentData, currentPage];
};

export const isDataDiff = (lastData, nextData) => {
  if (lastData.length !== nextData.length) return false;
  for (let i=0; i<lastData.length; i++) {
    if (JSON.stringify(lastData[i]) !== JSON.stringify(nextData[i])) {
      return true;
    }
  }
  return false;
};

export const isTableDataDiff = (lastData, nextData) => {
  if (lastData.length !== nextData.length) return true;

  // if length of 2 data are the same and lastData is empty, it means that nothing changed
  if (lastData.length === 0) return false;

  for (let i=0; i < lastData.length; i++) {
    if (isDataDiff(lastData[i], nextData[i])) {
      return true;
    }
  }
  return false
};

export default compose(
  defaultProps({
    tableHeaderColor: "gray",
    length: 5,
  }),
  setPropTypes({
    classes: PropTypes.object,
    tableHeaderColor: PropTypes.oneOf([
      "warning",
      "primary",
      "danger",
      "success",
      "info",
      "rose",
      "gray"
    ]),
    tableHead: PropTypes.arrayOf(PropTypes.string),
    tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.any))
  }),
  withState({
    currentData: [],
    currentPage: 0,
    totalPage: 0,
  }),
  withStyles(tableStyle),
  withStateHandlers(() => ({
    currentData: [],
    currentPage: 0,
    totalPage: 0,
  }), {
    switchPage: props => (page, data, length) => {
      const result = getDataAndPage(page, data, length);
      props.currentData = result[0];
      props.currentPage = result[1];
      return props;
    }
  }),
  lifecycle({
    componentWillUpdate(nextProps) {
      if (isTableDataDiff(this.props.tableData, nextProps.tableData) ||
        (nextProps.tableData.length > 0 && this.props.currentData.length === 0)) {
        this.props.switchPage(0, nextProps.tableData, this.props.length);
      }
    },
  })
)(TablePaginationLayout);

