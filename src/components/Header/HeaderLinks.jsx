import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import Search from "@material-ui/icons/Search";
// core components
import CustomInput from "../../components/CustomInput/CustomInput.jsx";
import Button from "../../components/CustomButtons/Button.jsx";

import headerLinksStyle from "../../assets/jss/material-dashboard-react/components/headerLinksStyle.jsx";
import Muted from "../Typography/Muted";

class HeaderLinks extends React.Component {
  state = {
    open: false
  };
  handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    return (
      <div style={{ position: "relative", display: "flex", alignItems: "center", height: "60px", marginBottom: "50px", background: "#fff", boxShadow: "0 1px 4px rgba(0, 21, 41, .08)", paddingBottom: "30px" }}>
        <div className={classes.logo} style={{ marginLeft: "30px", marginTop: "30px", display: "inline-flex", width: "400px" }}>
          <a href="/" className={classes.logoLink}>
            <div className={classes.logoImage}>
              <img src={`${window.location.origin}/favicon.ico`} alt="logo" className={classes.img} style={{ width: "50px" }}/>
            </div>
          </a>
          <Muted style={{ width: "300px", marginLeft: "10px" }}><h4>Lina network - blockchain explorer</h4></Muted>
        </div>
        <div className={classes.searchWrapper} style={{ width: "400px", marginLeft: "45%", marginTop: "30px" }}>
          <CustomInput
            formControlProps={{
              className: classes.margin + " " + classes.search
            }}
            inputProps={{
              placeholder: "Search Transaction Hash or Block Height",
              inputProps: {
                "aria-label": "Search Transaction Hash or Block Height",
                style: { width: "300px" }
              },
            }}
          />
          <Button color="white" aria-label="edit" justIcon round>
            <Search />
          </Button>
        </div>
      </div>
    );
  }
}

export default withStyles(headerLinksStyle)(HeaderLinks);
