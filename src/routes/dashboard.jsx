// @material-ui/icons
import HomeIcon from "@material-ui/icons/Home";
import DashboardIcon from "@material-ui/icons/Dashboard";
// import ContentPaste from "@material-ui/icons/ContentPaste";
// core components/views
import Dashboard from "../views/Dashboard";
import Transaction from "../views/Transactions";


const dashboardRoutes = [
  {
    path: "/home",
    sidebarName: "Home",
    navbarName: "Home",
    icon: HomeIcon,
    component: Dashboard
  },
  {
    path: "/transaction",
    sidebarName: "Transaction",
    navbarName: "Transaction",
    icon: DashboardIcon,
    component: Transaction
  },
  { redirect: true, path: "/", to: "/home", navbarName: "Redirect" },
];

export default dashboardRoutes;
