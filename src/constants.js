export const EXPLORER_API = process.env["EXPLORER"] ? process.env["EXPLORER"] : "http://localhost:8080";

export const GET_BLOCK_LIST = "getBlockList";
export const GET_TRANSACTION_LIST = "getTransactionList";
export const GET_TRANSACTION = "getTransaction";

export const BLOCK = {
  BLOCKS_LIST: "block/list",
  DOT_BLOCKS_LIST: "block.list"
};

export const TRANSACTION = {
  TRANSACTIONS_LIST: "transaction/list",
  DOT_TRANSACTIONS_LIST: "transaction.list",
  TRANSACTION_INFO: "transaction/info",
  DOT_TRANSACTION_INFO: "transaction.info"
};
